import requests
from django.shortcuts import render
from .models import City
from .forms import CityForm

# Create your views here.
def index(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=imperial&APPID=d6ab71fdc61f78b517f7a56db513f0c7'
    cities = City.objects.all()
    city_list = []

    if request.method == 'POST':
        form = CityForm(request.POST)
        form.save()

    form = CityForm()

    for  city in cities:
        r = requests.get(url.format(city)).json()
        weather = {
            'city' : r['name'],
            'temperature' : r['main']['temp'],
            'description' : r['weather'][0]['description'],
            'icon' : r['weather'][0]['icon'],
        }
        city_list.append(weather)

    context = {'city_list' : city_list, 'form' : form}
    return render(request, 'weather/index.html', context)
